﻿using OddTestingGrounds;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;


namespace MyTests {
    public class CalculatorTests {


        [Fact]

        public void Add_SumIsCorrectFactVersion() {

            // Arrangement
            double num1 = 13;
            double num2 = 7;

            double expectedResult = 20;

            // Acting
            var actualResult = Calculator.Add(num1, num2);

            // Asserting
            Assert.True(EqualWithErrorMargin(expectedResult, actualResult));

        }


        [Theory]
        [InlineData(13, 7, 20)]
        [InlineData(-13, -7, -20)]
        [InlineData(13.2, 6.8, 20.0)]

        public void Add_SumIsCorrectTheoryVersion(double num1, double num2, double expected) {

            var actual = Calculator.Add(num1, num2);

            Assert.True(EqualWithErrorMargin(expected, actual));

        }


        [Theory]
        [InlineData(10, 3, 7)]
        [InlineData(12, 15, -3)]
        [InlineData(32.4, 17.2, 15.2)]

        public void Subtract_SumIsCorrect(double num1, double num2, double expected) {

            var actual = Calculator.Subtract(num1, num2);

            Assert.True(EqualWithErrorMargin(expected, actual));

        }


        [Theory]
        [InlineData(3, 2, 6)]
        [InlineData(-4, 3, -12)]
        [InlineData(-5, -3, 15)]
        [InlineData(0.2, 0.8, 0.16)]

        public void Multiply_ProductIsCorrect(double num1, double num2, double expected) {

            var actual = Calculator.Multiply(num1, num2);

            Assert.True(EqualWithErrorMargin(expected, actual));

        }


        [Theory]
        [InlineData(10, 2, 5)]
        [InlineData(5, 10, 0.5)]
        [InlineData(0.2, 0.8, 0.25)]

        public void Divide_ProductIsCorrect(double num1, double num2, double expected) {

            var actual = Calculator.Divide(num1, num2);

            Assert.True(EqualWithErrorMargin(expected, actual));

        }


        [Fact]

        public void Divide_ThrowsDivisionByZeroException() {

            Action action = () => Calculator.Divide(8, 0);

            Assert.Throws<DivideByZeroException>(action);

        }


        public bool EqualWithErrorMargin(double num1, double num2) {
            return Math.Abs(num1 - num2) < 0.000001;
        }
    }
}
