﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddTestingGrounds {
    public class Calculator {

        public static double Add(double num1, double num2) {
            return num1 + num2;
        }

        public static double Subtract(double num1, double num2) {
            return num1 - num2;
        }

        public static double Multiply(double num1, double num2) {
            return num1 * num2;
        }

        public static double Divide(double num1, double num2) {
            if (num2 == 0)
                throw new DivideByZeroException(
                    "Division by zero!! Opening rift to another dimension");

            return num1 / num2;
        }
    }
}
